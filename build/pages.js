const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const config = require('./config')

module.exports = [
    new HTMLWebpackPlugin({
        filename: 'index.html',
        template: path.resolve(config.publicDir, 'index.twig')
    })
]