const path = require('path')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin')
const SVGSpriteLoaderPlugin = require('svg-sprite-loader/plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const pages = require('./pages')
const config = require('./config')
// const data = require('./dataLoad')(path.join(config.srcDir, 'data.json'))

const sourceMap = true

module.exports = {

    entry: {
        main: "./src/index.js"
    },

    output: {
        filename: '[name].[hash].js',
        path: config.distDir,
        publicPath: '/static/'
    },

    resolve: {
        alias: {
            assets: config.assetsDir,
            twig: config.publicDir,
        }
    },

    module: {
        rules: [
            {
                test: require.resolve('pixi.js-legacy/dist/pixi-legacy.min.js'),
                use: 'exports-loader?PIXI'
            },
            {
                test: require.resolve('pixi.js/dist/pixi.min.js'),
                use: 'exports-loader?PIXI'
            },
            {
                test: /\.twig$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            interpolate: true
                        }
                    },
                    {
                        loader: 'twig-html-loader',
                        options: {
                            filters: {
                                dateScreens(screens) {
                                    return screens.reduce((acc, screen) => {
                                        const hasNumber = /^\d+$/.test(screen.label)
                                        const notExistsYet = !acc.find(i => i.label === screen.label)
                                        if (hasNumber && notExistsYet) {
                                            acc.push(screen)
                                        }
                                        return acc
                                    }, [])
                                }
                            },
                            functions: {
                                getEnv() {
                                    return process.env.NODE_ENV
                                }
                            }
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                    // options: {
                    // attrs: [':src', ':xlink:href']
                    // }
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    {loader: MiniCssExtractWebpackPlugin.loader},
                    {loader: 'css-loader', options: {sourceMap}},
                    {loader: 'postcss-loader', options: {sourceMap}},
                    {loader: 'sass-loader', options: {sourceMap}}
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                include: path.resolve(config.assetsDir, 'images'),
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'images',
                        },
                    },
                    // {
                    //     loader: 'image-webpack-loader',
                    // }
                ]
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {outputPath: 'fonts'}
                    }
                ]
            },
            {
                test: /\.svg$/,
                include: path.resolve(config.assetsDir, 'icons'),
                use: [
                    {
                        loader: 'svg-sprite-loader',
                        options: {
                            extract: true
                        },
                    },
                    {
                        loader: 'svgo-loader',
                    }
                ]
            }
        ]
    },

    plugins: [
        ...pages,
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: ['!c/**/*']
        }),
        new MiniCssExtractWebpackPlugin({
            filename: '[name].[hash].css'
        }),
        new SVGSpriteLoaderPlugin({
            plainSprite: true,
            spriteAttrs: {
                style: 'position: absolute; visibility: hidden; width: 0; height: 0'
            }
        }),
        new CopyWebpackPlugin([
            { from: path.resolve(config.assetsDir, 'copy/'), to: path.resolve(config.distDir, 'c') }
        ])
    ]
}