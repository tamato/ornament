const path = require('path')
const merge = require('webpack-merge')
const base = require('./base.config')
const config = require('./config')

module.exports = merge(base, {
    // devServer: {
    //     open: true,
    //     port: 9000,
    // },

    // cheap-module-source-map is this issue https://github.com/webpack-contrib/mini-css-extract-plugin/issues/29
    devtool: 'cheap-module-source-map',
})
