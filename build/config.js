const path = require('path')

const distDir = path.join(__dirname, '/../web/static')
const publicDir = path.join(__dirname, '/../public')
const srcDir = path.join(__dirname, '/../src')
const assetsDir = path.join(srcDir, 'assets')

module.exports = {
    distDir,
    assetsDir,
    publicDir,
    srcDir
}