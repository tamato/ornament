module.exports = function (jsonFilePath) {
    return function(context) {
        context.addDependency(jsonFilePath)
        return context.fs.readJsonSync(jsonFilePath, { throws: false }) || {}
    }
}