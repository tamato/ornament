const path = require('path')
const merge = require('webpack-merge')
const base = require('./base.config')
const config = require('./config')

module.exports = merge(base, {
    devtool: false
})
