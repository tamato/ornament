import { select } from "./utils/index"
import { nav } from './navigation'
import scroll from './scroll'

const MENU_OPEN_CLASS = 'open'
const TOGGLE_OPEN_CLASS = 'open'
const BODY_NOSCROLL_CLASS = 'noscroll'

let menu
let toggle
let isOpen = false
let body = document.body
let menuItems

function toggleMenu () {
    if (isOpen) {
        close()
    } else {
        open()
    }
}

function open () {
    menu.classList.add(MENU_OPEN_CLASS)
    toggle.classList.add(TOGGLE_OPEN_CLASS)
    body.classList.add(BODY_NOSCROLL_CLASS)
    isOpen = true
}

function close () {
    menu.classList.remove(MENU_OPEN_CLASS)
    toggle.classList.remove(TOGGLE_OPEN_CLASS)
    body.classList.remove(BODY_NOSCROLL_CLASS)
    isOpen = false
}

function init () {
    menu = select.one('#mobile-menu')
    toggle = select.one('.panel__toggle')
    menuItems = select.all('[data-mmenu-item]')

    menuItems.forEach(item => {
        item.addEventListener('click', e => {
            // e.preventDefault()
            const sectionName = item.getAttribute('data-mmenu-item')
            // nav.toSection(sectionName)
            const to = '[data-anchor="' +  sectionName + '"]'
            scroll.to(select.one(to))
            close()
        })
    })

    toggle.addEventListener('click', toggleMenu)
}

export default { init }