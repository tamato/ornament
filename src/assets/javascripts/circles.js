import PIXI from 'pixi.js-legacy/dist/pixi-legacy.min'
import debounce from 'lodash.debounce'
import gsap from 'gsap'

let params = { w: null, h: null }

const DURATION_DEFAULT = 4000

function refreshParams () {
    params.w = document.body.offsetWidth
    params.h = window.innerHeight
}

const resize = debounce(() => {
    refreshParams()
}, 200)

function init (duration = DURATION_DEFAULT) {
    refreshParams()

    const loader = PIXI.Loader.shared

    loader.add('circle1', '/static/c/1.png')
    loader.add('circle2', '/static/c/2.png')
    loader.add('circle3', '/static/c/3.png')
    loader.add('circle4', '/static/c/4.png')

    window.addEventListener('resize', () => {
        refreshParams()
        app.renderer.resize(params.w, params.h)
    })

    const app = new PIXI.Application({
        width: params.w,
        height: params.h,
        transparent: true,
        view: document.querySelector('#canvas')
    })

    document.body.appendChild(app.view)

    let container = new PIXI.Container()

    function createCircle (item , idx) {
        const size = app.screen.height/4 - idx * 30
        return new PIXI.Graphics()
            .beginFill(item.color)
            .drawCircle(0, 0, size)
            .endFill()
    }

    let circles = []



    // const circleBlur = new PIXI.filters.BlurFilter(90, 16)

    // container.filters = [circleBlur]

    app.stage.addChild(container)

    let x = 0,
        y = 1

    function random (max, min = 0) {
        return Math.floor(Math.random() * (max - min + 1)) + min
    }

    function moveTo (x, y, duration = DURATION_DEFAULT/1000) {
        circles.forEach((c, idx) => {
            const fast = duration - duration * idx/15
            gsap.to(c, fast, { x, y, ease: 'power1.inOut' })
        })
    }

    function moveRandom () {
        const dur = duration/1000
        x = random(app.screen.width)
        y = random(app.screen.height)

        moveTo(x, y, dur)

        return setInterval(() => {
            const dur = duration/1000
            x = random(app.screen.width)
            y = random(app.screen.height)

            moveTo(x, y, dur)
        }, duration)
    }

    // moveTo(app.screen.width/2, app.screen.height/2)

    let movement

// app.ticker.add(() => {
//   circlePink.position.set(x, y)
//   circle.position.set(x, y)
// })

    return new Promise(resolve => {

        loader.load(() => {
            circles = [
                PIXI.Sprite.from('circle1'),
                PIXI.Sprite.from('circle2'),
                PIXI.Sprite.from('circle3')
            ].reverse()

            circles.forEach(c => {
                c.anchor.set(0.5)
                container.addChild(c)
            })

            moveTo(-500, -500, 0)
            moveTo(0, 0, 3)

            resolve({
                stop () {
                    this.reset()
                    moveTo(0, 0)
                },
                reset () {
                    clearInterval(movement)
                },
                move (timeout) {
                    movement = moveRandom()

                    if (timeout) {
                        setTimeout(() => {
                            this.stop()
                        }, timeout)
                    }
                }
            })
        })
    })
}

export default {
    init
}