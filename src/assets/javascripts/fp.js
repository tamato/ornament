import FullPage from 'fullpage.js'

function init () {
    let fp = new FullPage('#fp', {
        sectionSelector: '[data-fp-section]',
        menu: '[data-nav]',
        scrollingSpeed: 1200,
        onLeave () {
            document.dispatchEvent(new CustomEvent('nav:toscreen'))
        }
    })

    return fp
}

export default {
    init
}