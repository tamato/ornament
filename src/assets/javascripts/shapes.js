const Snap = require(`imports-loader?this=>window,fix=>module.exports=0!snapsvg/dist/snap.svg-min.js`);
import './utils/easings'

let s

const paths = {
    thin: 'm117.733 237.385C23.0526 130.162 -2.07301 63.98 0.130618 0C67.045 6.28212 23.0519 92.4333 117.732 199.656C212.412 306.878 391.401 353.926 389.461 413.671C334.158 437.157 212.413 344.607 117.733 237.385z',
    ball: 'm38.3942 86.3387C71.3954 49.8386 245.894 -51.1613 362.894 33.3387C479.894 117.839 432.501 356.5 399.5 368C366.499 379.5 103.894 453.339 38.3942 391.34C-27.1058 329.34 5.39305 122.839 38.3942 86.3387z',
    triangle: 'm22.9512 224.566C-39.5488 181.066 41.9512 47.5651 99.4512 9.06514C148.451 -39.8899 330.951 145.566 363.451 251.566C395.951 357.566 85.4512 268.066 22.9512 224.566z',
    boomerang: 'm271.61 318.5C271.61 461.542 116.663 640.501 31.6107 640.501C-53.4411 640.501 60.0717 461.542 60.0717 318.5C60.0717 175.459 -7.4423 0 77.6096 0C162.661 0 271.61 175.459 271.61 318.5z',
    drop: 'm117.622 147.573C92.3687 75.7344 -50.1573 -0.901939 19 1.49987C87.3776 -9.55002 362.298 83.5069 326.663 188.495C291.029 293.482 142.876 219.411 117.622 147.573z',
    thin2: 'm114.091 312.001C114.091 455.043 57.0918 493.001 13.0914 539.501C-32.9087 490.501 57.0919 455.043 57.0919 312.001C57.0919 168.959 -13.9081 43.5 27.0919 0C84.0919 19 114.091 168.959 114.091 312.001z',
    circle: 'm456.5 232C456.5 360.13 341.13 460.788 213 460.788C84.8699 460.788 0 375.13 0 247C0 118.87 116.87 0 245 0C373.13 0 456.5 103.87 456.5 232z',
    wtf: 'm201.5 291C106.82 183.778 -1.20562 254.98 0.998008 191C63.8674 120.001 250.316 -75.2222 344.996 32.0002C439.677 139.222 526.76 467.184 471.457 490.67C416.154 514.157 296.18 398.222 201.5 291z',
    spot1: "M456.5 232C456.5 360.13 341.13 460.788 213 460.788C84.8699 460.788 0 375.13 0 247C0 118.87 116.87 0 245 0C373.13 0 456.5 103.87 456.5 232Z",
    spot2: "M427.5 342.001C427.5 470.131 341.13 555.789 213 555.789C84.87 555.789 5.97603e-05 470.131 5.97603e-05 342.001C5.97603e-05 213.871 -0.630005 0.5 127.5 0.5C255.63 0.5 427.5 213.871 427.5 342.001Z",
    spot3: "M355 152.903C355 250.131 223.981 347 126.575 347C29.1688 347 0 223.79 0 126.563C0 29.3349 94.5286 0 191.935 0C289.341 0 355 55.6751 355 152.903Z",
    spot4: "M281 200.732C281 277.507 177.474 354 100.508 354C23.5413 354 0.493232 256.708 0.493232 179.933C0.493232 103.157 -10.0973 0 66.8691 0C143.835 0 281 123.957 281 200.732Z",
    spot5: "M278.522 8.10179C348.341 40.0359 374.841 165.998 342.828 235.991C310.814 305.983 212.751 286.475 142.932 254.541C73.1135 222.607 -25.1018 189.33 6.91174 119.338C38.9253 49.3451 208.703 -23.8323 278.522 8.10179Z",
}

const track = 'M1 1C81.3333 118.167 275.5 354.8 409.5 364C577 375.5 654 199 770 205C886 211 1070 366.5 979.5 545.5C889 724.5 600 775.5 429.5 721.5C259 667.5 1 216.5 1 120'

const colors = []

const pathsList = Object.entries(paths).map(([key, path]) => path)

function getRandomShape () {
    const max = pathsList.length - 1
    const idx = random(max)
    return pathsList[idx]
}

function random (max, min = 0) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function makeShape (form, invertGradient = false) {
    const path = s.path(form)
    const color = invertGradient ? 'l(0, 0, 1, 1)#000-#f518a7' : 'l(0, 0, 1, 1)#f518a7-#000'
    const radialGradient = 'r(0.5, 0.5, 0.5)#000-#f518a7'
    path.attr({ fill: 'green' })
    path.attr({ fill: s.gradient(color) })
    return path
}

function makeShapes (count, form) {
    const g = s.group()
    for(let i = 0; i < count; i++) {
        const path = makeShape(form)
        path.attr({ transform: `s${1 - (i * 0.2)}` })
        g.add(path)
    }
    return g.selectAll('path')
}

function init () {
    const svg = document.querySelector('#shapes')
    if (!svg) return
    s = Snap('#shapes')
    const w = document.body.offsetWidth
    const h = window.innerHeight
    const screen = { x1: w * 0.3, y1: h * 0.3, x2: w -  w * 0.3, y2: h - h * 0.3 }
    const shape = makeShape(paths.ball)
    s.add(shape)
    const duration = 4000
    // const shapes = makeShapes(4, paths.ball)
    const group = s.group(shape)
    const gradient = s.gradient('r(0.5, 0.5, 1)#DD9DCF-#D8BCE0-#BBE2F1-#DBF2F8')

    shape.attr({ fill: gradient })

    let prevShape
    function shaping (duration) {
        const d = getRandomShape(paths)
        if (prevShape === d) {
            shaping(duration)
        } else {
            prevShape = d
            shape.animate({ d }, duration, mina.easeInOutCubic)
        }
    }
    let shapingDuration = 3000
    let shapingInterval

    function randomMovement (duration) {
        const x = Math.floor(Math.random() * 100) / 100
        const y = Math.floor(Math.random() * 100) / 100
        const moveX = w * x - w * 0.2
        const moveY = h * y - h * 0.2
        const transform = `T${moveX},${moveY}`
        group.animate({ transform }, duration, mina.easeinout)
        gradient.animate({ cx: x, cy: y }, duration, mina.easeinout)
    }

    let moveDuration = 4000
    let moveTimeout

    function parking () {
        clearInterval(moveTimeout)
        moveTimeout = null

        clearInterval(shapingInterval)
        shapingInterval = null

        gradient.animate({ cx: 0.5, cy: 0.5 }, 3000, mina.easeinout)
        group.animate({ transform: 'T-100,-100' }, duration, mina.easeinout)
        shape.animate({ d: paths.spot3 }, 3000)
    }

    let walkTimeout
    function walk (time) {
        if (!moveTimeout) {
            randomMovement(moveDuration)
            moveTimeout = setInterval(() => {
                randomMovement(moveDuration)
            }, moveDuration + 1)
        }

        if (!shapingInterval) {
            shaping(shapingDuration)
            shapingInterval = setInterval(() => {
                shaping(shapingDuration)
            })
        }

        if (time) {
            clearTimeout(walkTimeout)
            walkTimeout = setTimeout(() => {
                parking()
            }, time)
        }
    }

    walk()

    return { walk, parking }
}

export default { init }