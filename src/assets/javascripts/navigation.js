import { select } from "./utils/index"
import scroll from './scroll'
import { SPEED as SCROLL_SPEED } from "./scroll"
import responsive from './utils/responsive'
import debounce from 'lodash.debounce'

class Nav {

    toSection (name) {
        const section = this.sections.find(section => section.getAttribute('data-section') === name)
        if (!section) return
        this.scrollTo(section)
    }

    scrollTo (element) {
        if (this.scrolling) return
        document.dispatchEvent(new CustomEvent('nav:toscreen', { detail: element.getAttribute('data-section') }))
        this.scrolling = true
        scroll.to(element)
        setTimeout(() => (this.scrolling = false), SCROLL_SPEED)
    }

    toScreen (idx) {
        this.scrollTo(this.screens[idx])
    }

    constructor () {
        return
        this.screens = select.all('.screen')
        this.sections = select.all('[data-section]')
        this.buttons = select.all('[data-nav-item]')
        this.screenIdx = null
        this.sectionName = null
        this.scrolling = false

        this.buttons.forEach(button => {
            button.addEventListener('click', e => {
                e.preventDefault()
                const name = button.getAttribute('data-nav-item')
                const element = this.sections.find(s => s.getAttribute('data-section') === name)
                if (!element) return
                this.scrollTo(element)
            })
        })


        this.fullPage(!responsive.isMobile())
        document.addEventListener('resized', e => {
            // console.log(this.isFullPage)
            this.fullPage(!responsive.isMobile())
        })

        this.watch()
    }

    fullPage (state = true) {
        const wheel = debounce((e) => {
            const dir = e.deltaY > 0 ? 'next' : 'prev'
            this[dir]()
        }, 250, { leading: true, trailing: false })

        if (!!state) {
            if (this._fullPageBinded) return
            this._fullPageBinded = function (event) {
                event.preventDefault()
                event.stopPropagation()

                // workaround for inertial scroll
                if (Math.abs(event.deltaY) < 25) return

                wheel(event)
            }
            window.addEventListener('wheel', this._fullPageBinded, { passive: false })
        } else {
            if (!this._fullPageBinded) return
            window.removeEventListener('wheel', this._fullPageBinded)
            this._fullPageBinded = null
        }
    }

    setIdx (idx) {
        this.screenIdx = idx
    }

    prev () {
        const prevIdx = this.screenIdx - 1
        if (prevIdx < 0) return
        this.toScreen(prevIdx)
    }

    next () {
        const nextIdx = this.screenIdx + 1
        if (nextIdx > (this.screens.length - 1)) return
        this.toScreen(nextIdx)
    }

    activateButton (name) {
        const button = this.buttons.find(button => button.getAttribute('data-nav-item') === name)
        if (!button) return
        this.buttons.forEach(b => b.classList.remove('active'))
        button.classList.add('active')
    }

    watch () {
        let observer = new IntersectionObserver((entries) => {
            entries.forEach(e => {
                if (e.isIntersecting) {
                    const idx = this.screens.findIndex(screen => screen === e.target)
                    const name = e.target.getAttribute('data-section')
                    if (idx > -1) {
                        this.setIdx(idx)
                    }
                    if (name) {
                        this.sectionName = name
                        this.activateButton(name)
                    }
                }
            })
        }, { threshold: 0, rootMargin: '-50% 0px' })

        this.screens.forEach(screen => observer.observe(screen))
        this.sections.forEach(section => observer.observe(section))
    }
}

export const nav = new Nav()

function init () {
    return nav
}

export default { init }