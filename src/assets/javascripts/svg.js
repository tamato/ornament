const Snap = require(`imports-loader?this=>window,fix=>module.exports=0!snapsvg/dist/snap.svg-min.js`);

function init () {
    const svg = document.querySelector('#svg')
    if (!svg) return
    const w = document.body.offsetWidth
    const h = window.innerHeight
    const radiusSize = w * 0.05

    const s = Snap(svg)

    const circles = s.selectAll('circle')

    circles.items.slice().reverse().forEach((item, idx) => {
        const i = idx + 1
        const radius = radiusSize + (i * radiusSize * 0.3)
        item.attr({ r: radius })
    })

    function random (max, min = 0) {
        return Math.ceil(Math.random() * (max - min))
    }

    function moveRandom (el, duration) {
        to(el, random(w/2), random(h/2), duration)
    }

    function to (el, x, y, duration = 500) {
        const angle = random(90)
        el.animate({ transform: `t${x},${y}R${angle}` }, duration, mina.easeinout)
    }

    function getShape () {
        const list = Object.entries(shapes).map(([key, path]) => path)
        const max = list.length
        const idx = random(max)
        return list[idx - 1]
    }

    function animate (interval) {
        const items = circles.items.reverse()

        function go (x, y, scale, duration) {
            items.forEach((item, idx) => {
                // const currentX = item.attr('cx')
                // const currentY = item.attr('cy')
                // const deltaX = Math.abs(x - currentX)
                // const deltaY = Math.abs(y - currentY)
                // const pathLength = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2))
                const delay = idx * 100
                setTimeout(() => {
                    item.animate({
                        cx: x ,
                        cy: y,
                    }, duration, mina.easeinout)
                }, delay)
            })
        }

        setInterval(() => {
            const x = (w * 0.5/2) + random(w * 0.5)
            const y = (h * 0.5/2) + random(h * 0.5)
            const duration = interval
            const scale = `s${random(2)}`
            go(x, y, scale, duration)
        }, interval)

        function scaling () {
            const transform = `s${random(100)/100 + 1}`
            circles.items.slice().reverse().forEach((item, idx) => {
                setTimeout(() => {
                    item.animate({ transform }, 2000)
                }, idx * 100)
            })
        }

        setInterval(() => {
            scaling()
        }, 1000)
    }

    animate(3000)
}

export default { init }