const Snap = require(`imports-loader?this=>window,fix=>module.exports=0!snapsvg/dist/snap.svg-min.js`);

export default class Spot {
    random (max, min = 0) {
        return Math.floor(Math.random() * (max - min + 1)) + min
    }

    getForms () {
        return {
            spot1: "M456.5 232C456.5 360.13 341.13 460.788 213 460.788C84.8699 460.788 0 375.13 0 247C0 118.87 116.87 0 245 0C373.13 0 456.5 103.87 456.5 232Z",
            spot2: "M427.5 342.001C427.5 470.131 341.13 555.789 213 555.789C84.87 555.789 5.97603e-05 470.131 5.97603e-05 342.001C5.97603e-05 213.871 -0.630005 0.5 127.5 0.5C255.63 0.5 427.5 213.871 427.5 342.001Z",
            spot3: "M355 152.903C355 250.131 223.981 347 126.575 347C29.1688 347 0 223.79 0 126.563C0 29.3349 94.5286 0 191.935 0C289.341 0 355 55.6751 355 152.903Z",
            spot4: "M281 200.732C281 277.507 177.474 354 100.508 354C23.5413 354 0.493232 256.708 0.493232 179.933C0.493232 103.157 -10.0973 0 66.8691 0C143.835 0 281 123.957 281 200.732Z",
            spot5: "M278.522 8.10179C348.341 40.0359 374.841 165.998 342.828 235.991C310.814 305.983 212.751 286.475 142.932 254.541C73.1135 222.607 -25.1018 189.33 6.91174 119.338C38.9253 49.3451 208.703 -23.8323 278.522 8.10179Z",
        }
    }

    getFormsList () {
        return Object.entries(paths).map(([key, path]) => path)
    }

    getRandomShape (shapes) {
        const max = shapes.length - 1
        const idx = this.random(max)
        return shapes[idx]
    }

    defaults () {
        return {
            initialForm: this.getFormsList()[0]
        }
    }

    mergeOptions (opts) {
        Object.keys(this.defaults).reduce((acc, [key, idx]) => (this[key] = opts[key]), {})
    }

    constructor (options) {
        this.mergeOptions()
        this.$el = this.render()
    }

    render () {
        const shape = Snap.shape()
        const rotate = Snap.group()
        const position = Snap.group(rotate)
        shape.attr({ d: this.initialForm })
        return position
    }
}