export const debounce = require('./debounce').default
export const select = require('./select').default

export default { debounce, select }