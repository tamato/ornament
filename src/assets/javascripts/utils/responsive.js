import { debounce } from "./index"

export const BREAKPOINTS = { MOBILE: 1024 }

export let params

export function calc () {
    const w = document.body.offsetWidth
    const h = window.innerHeight
    const isMobile = w < BREAKPOINTS.MOBILE
    return { w, h, isMobile }
}

const onResize = debounce(() => {
    params = calc()
    document.dispatchEvent(new CustomEvent('resized', { detail: params }))
}, 250)

function init () {
    window.addEventListener('resize', onResize)
    params = calc()
}

init()

export default {
    params,
    isMobile() {
        return params.isMobile
    }
}