export function selectOne(selector, parent = document, all = false) {
    const elements = [].map.call(parent.querySelectorAll(selector), el => el)
    return all ? elements : elements[0]
}

export function selectAll(selector, parent = document) {
    return selectOne(selector, parent, true)
}

export default { all: selectAll, one: selectOne }