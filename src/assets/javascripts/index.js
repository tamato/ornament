import { select } from "./utils/index"
import svg4everybody from 'svg4everybody/dist/svg4everybody.min'
import svg from './svg'
// import shapes from './shapes'
import scroll from './scroll'
import responsive from './utils/responsive'
import navigation from './navigation'
import mobileMenu from './mobile-menu'
import fp from './fp'
import circles from './circles'
import metrics from './metrics'

const isDev = process.env.NODE_ENV === 'development'

svg4everybody()

select.all('[data-current-year]').forEach(el => {
    el.innerHTML = new Date().getFullYear()
})

if (!isDev) {
    select.all('[data-goal]').forEach(el => {
        const data = el.getAttribute('data-goal').replace(/'/g, "\"")
        el.addEventListener('click', e => {
            metrics.goal(JSON.parse(data))
        })
    })
}

window.addEventListener('load', e => {
    let fullpage

    if (responsive.isMobile()) hidePanel()

    select.all('.js-wawd-more').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault()
            const already = el.parentNode.classList.contains('clicked')
            if (already) return
            el.parentNode.classList.add('clicked')
            setTimeout(() => {
                el.parentNode.classList.add('click-animated')
            }, 1000)
        })
    })

    function scrollTo (sectionName) {
        if (fullpage) {
            fullpage.moveTo(sectionName)
        } else {
            scroll.to(select.one('[data-section="' + sectionName + '"]'))
        }
    }

    select.all('[data-scroll-to]').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault()
            scrollTo(el.getAttribute('data-scroll-to'))
        })
    })
    svg.init()
    // const sh = shapes.init()
    let c
    circles.init().then((api) => {
        c = api
        check(responsive.params)
    })

    function check (params) {
        if (params.isMobile) {
            // sh.walk()
            c.move()
            if (fullpage) {
                fullpage.destroy('all')
                fullpage = null
            }
        } else {
            !fullpage && (fullpage = fp.init())
            c.move(12000)
            // sh.walk(12000)
            document.addEventListener('nav:toscreen', e => {
                c.reset()
                c.move(12000)
                // sh.walk(12000)
            })
        }
    }

    document.addEventListener('resized', e => check(e.detail))

    mobileMenu.init()
})


function hidePanel () {
    const panel = select.one('.panel')
    let scrollPos = 0
    window.addEventListener('scroll', e => {
        const pos = window.pageYOffset
        const offset = pos - scrollPos
        const dir = offset < 0 ? 'top' : 'down'
        if (dir === 'down') {
            panel.classList.add('hidden')
        } else {
            panel.classList.remove('hidden')
        }
        scrollPos = pos
    })
}

function watchResponsive () {
    const MOBILE_CLASS = 'is-mobile'
    const DESKTOP_CLASS = 'is-desktop'

    function toggleClases (params) {
        if (params.isMobile) {
            document.body.classList.add(MOBILE_CLASS)
            document.body.classList.remove(DESKTOP_CLASS)
        } else {
            document.body.classList.add(DESKTOP_CLASS)
            document.body.classList.remove(MOBILE_CLASS)
        }
    }

    toggleClases(responsive.params)

    document.addEventListener('resized', e => {
        toggleClases(e.detail)
    })

}

watchResponsive()