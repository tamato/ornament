const YM_ID = '61866846'
// const GA_ID = 'UA-163305130-1'
const isDev = process.env.NODE_ENV === 'development'

function goalGA (name) {
    window.gtag('event', 'goal', { event_label: name })
}

function goalYM (name, params = {}) {
    if (isDev) {
        console.log(name, params)
    }
    window.ym(YM_ID, 'reachGoal', name, params)
}

export default {
    goal (data) {
        goalYM(data.name, data.params)
        goalGA(data.name)
    }
}