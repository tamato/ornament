import SmoothScroll from 'smooth-scroll'
import { select } from "./utils/index"

export const SPEED = 1000

const scroll = new SmoothScroll(null, { speedAsDuration: true, speed: SPEED });

export default {
    to (target, speed = SPEED) {
        const position = typeof target === 'object' ? target.getBoundingClientRect().top + pageYOffset : target
        scroll.animateScroll(position, null, { speed })
    }
}