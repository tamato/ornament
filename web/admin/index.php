<?php
    require(__DIR__ . '/../main.php');

    const USER_NAME = 'admin';
    const USER_PWD = '123';
    const FILENAME = __DIR__ . '/../data.json';

    function askCredentials() {
        header('WWW-Authenticate: Basic realm="admin"');
        header('HTTP/1.0 401 Unauthorized');
        exit;
    }

    function checkAuth() {
        if ($_SERVER['PHP_AUTH_USER'] !== USER_NAME || $_SERVER['PHP_AUTH_PW'] !== USER_PWD) askCredentials();
    }

    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        askCredentials();
    } else {
        checkAuth();
    }

    $postData = file_get_contents('php://input');
    $postData = json_decode($postData, true);

    if (isset($postData['json']) && $postData['json']) {
        $content = json_encode($postData['json']);
        file_put_contents(FILENAME, $content);
        header('Content-type: application/json');
        exit();
    }
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/7.0.4/jsoneditor.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/7.0.4/jsoneditor.min.js"></script>
    <style>
        body {
            margin: 0;
            padding: 0;
        }
        .dashboard {
            display: flex;
            flex-direction: column;
            height: 100vh;
        }
        .dashboard__top {
            flex: 0 0 auto;
            height: 50px;
            padding: 0 20px;
            display: flex;
            align-items: center;
        }
        .dashboard__editor {
            flex: 1 1 100%;
            height: 100%;
        }
        .submit {
            border-radius: 4px;
            padding: 10px;
            border: 0;
            background: #3883fa;
            color: #fff;
            min-width: 100px;
            cursor: pointer;
        }
        .submit:hover {
            background: #1860d4;
        }
        .submit:disabled {
            opacity: 0.4;
            pointer-events: none;
        }
        .status.info {
            color: #888888;
        }
        .status.success {
            color: #54ad1c
        }
        .status.error {
            color: #ff2000;
        }
    </style>
</head>
<body>
    <div class="dashboard">
        <div class="dashboard__top">
            <button id="saveButton" class="submit">Save</button>
            <span id="status" class="status" style="margin-left: 30px"></span>
        </div>
        <div id="editor" class="dashboard__editor"></div>
    </div>

    <script>
        const MSG_JSON_NOT_VALID = 'Not valid JSON! Check errors.'
        const MSG_SERVER_ERROR = 'Server error!'
        const MSG_SAVED = 'Saved!'

        const data = JSON.parse(`<?= json_encode($data) ?>`)
        const editor = new JSONEditor(document.querySelector('#editor'), { mode: 'code', onChange: validate })
        const saveButton = document.querySelector('#saveButton')
        const statusEl = document.querySelector('#status')

        function validate () {
            try {
                editor.get()
                saveButton.disabled = false
                status('')
            } catch (error) {
                saveButton.disabled = true
                status(MSG_JSON_NOT_VALID, 'error')
            }
        }

        function status (message, type = 'info', timeout = false) {
            statusEl.innerHTML = message
            if (type) {
                statusEl.classList.add(type)
            }
            if (!message) {
                statusEl.className = 'status'
            } else {
                console.log(message)
            }
            if (timeout && message) {
                setTimeout(() => {
                    status('')
                }, timeout)
            }
        }

        editor.set(data)

        saveButton.addEventListener('click', () => post(editor.get()))

        function post (data) {
            fetch('/admin/index.php', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ json: data })
            })
                .then((response) => {
                    if(response.ok) {
                        status(MSG_SAVED, 'success', 1500)
                    } else {
                        status(MSG_SERVER_ERROR + '. ' + response.statusText, 'error')
                    }
                })
                .catch(err => {
                    status(err.message, 'error')
                })
        }
    </script>
</body>
</html>
