<?php

    $file = file_get_contents(__DIR__ . '/data.json');
    $data = json_decode($file, true);

    $contacts = [
         [
            "id" => t("socials.telegram.id"),
            "icon" => "telegram",
            "link" => t("socials.telegram.link"),
            "name" => t("socials.telegram.name")
        ],
        [
            "id" => t("socials.whatsapp.id"),
            "icon" => "whatsapp",
            "link" => t("socials.whatsapp.link"),
            "name" => t("socials.whatsapp.name")
        ],
        [
            "id" => t("socials.email.id"),
            "icon" => "gmail",
            "mailto" => t("socials.email.mailto"),
            "link" => t("socials.email.link"),
            "name" => t("socials.email.name")
        ]
    ];

    function getDataItem($selector, $data) {
        if (!$selector) return $data;
        $arr = explode('.', $selector);
        $key = $arr[0];
        $value = $data[$key];
        $tail = implode('.', array_slice($arr, 1));
        if (gettype($value) == 'array') {
            return getDataItem($tail, $value);
        }
        return $value;
    }

    function t($key) {
        global $data;
        return getDataItem($key, $data);
    }
